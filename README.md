# Gitlab Pipeline Templates

Easy way to include gitlab templates into your gitlab ci/cd.

Base: https://gitlab.com/pipeline-components

## Example

include:
  - remote: 'https://xxx/gitlab-pipeline-templates/master/linter/xml.yaml'
